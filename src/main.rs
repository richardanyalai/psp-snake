#![no_std]
#![no_main]

psp::module!("Snake", 1, 0);

use psp::sys::{
	CtrlButtons,
	CtrlMode,
	SceCtrlData
};

mod gfx;
mod snake;

fn psp_main() {
	psp::enable_home_button();

	unsafe {
		psp::sys::sceCtrlSetSamplingCycle(0);
		psp::sys::sceCtrlSetSamplingMode(CtrlMode::Analog);

		let mut snake = snake::Snake::new();

		let ctrl_data = &mut SceCtrlData::default();

		loop {
			psp::sys::sceCtrlReadBufferPositive(ctrl_data, 1);

			if ctrl_data.buttons.contains(CtrlButtons::UP)
			&& snake.dir != snake::DIRECTION::DOWN {
				snake.dir = snake::DIRECTION::UP;
			}
			else if ctrl_data.buttons.contains(CtrlButtons::DOWN)
			&& snake.dir != snake::DIRECTION::UP {
				snake.dir = snake::DIRECTION::DOWN;
			}
			else if ctrl_data.buttons.contains(CtrlButtons::LEFT)
			&& snake.dir != snake::DIRECTION::RIGHT {
				snake.dir = snake::DIRECTION::LEFT;
			}
			else if ctrl_data.buttons.contains(CtrlButtons::RIGHT)
			&& snake.dir != snake::DIRECTION::LEFT {
				snake.dir = snake::DIRECTION::RIGHT;
			}

			snake.update();
			snake.draw();

			psp::sys::sceKernelDelayThread(100000);
		}
	}
}
