use crate::gfx::Renderer;

#[derive(PartialEq)]
pub enum DIRECTION {
	UP,
	DOWN,
	LEFT,
	RIGHT,
}

const TILE_SIZE: i32 = 16;
const WIDTH: i32 = psp::SCREEN_WIDTH as i32 / TILE_SIZE;
const HEIGHT: i32 = psp::SCREEN_HEIGHT as i32 / TILE_SIZE;

pub struct Snake {
	renderer: Renderer,
	pub dir: DIRECTION,
	length: usize,
	body: [(i32, i32); (WIDTH * HEIGHT) as usize],
	apple: (i32, i32),
}

impl Snake {
	pub unsafe fn new() -> Self {
		let renderer = Renderer::new();
		let dir = DIRECTION::RIGHT;
		let length = 3;
		let mut body = [(0, 0); (WIDTH * HEIGHT) as usize];
		let apple = (5, 5);

		body[0].0 = 3;
		body[0].1 = 3;
		body[1].0 = 2;
		body[1].1 = 3;
		body[2].0 = 1;
		body[2].1 = 3;

		Self {
			renderer,
			dir,
			length,
			body,
			apple,
		}
	}

	pub fn update(&mut self) {
		let mut apple_eaten = false;

		if self.body[0].0 == self.apple.0 && self.body[0].1 == self.apple.1 {
			apple_eaten = true;
			self.length += 1;
		}

		for i in 1..self.length {
			if self.body[0].0 == self.body[self.length - i].0
				&& self.body[0].1 == self.body[self.length - i].1
			{
				self.body[2].0 = self.body[1].0;
				self.body[2].1 = self.body[1].1;
				self.body[1].0 = self.body[0].0;
				self.body[1].1 = self.body[0].1;
				self.length = 3;
				break;
			}

			self.body[self.length - i].0 = self.body[self.length - i - 1].0;
			self.body[self.length - i].1 = self.body[self.length - i - 1].1;
		}

		if apple_eaten {
			self.apple.0 = self.body[self.length - 1].0;
			self.apple.1 = self.body[self.length - 1].1;
		}

		match self.dir {
			DIRECTION::UP => self.body[0].1 -= 1,
			DIRECTION::DOWN => self.body[0].1 += 1,
			DIRECTION::LEFT => self.body[0].0 -= 1,
			DIRECTION::RIGHT => self.body[0].0 += 1,
		}

		if self.body[0].1 < 0 {
			self.body[0].1 = HEIGHT - 1;
		} else if self.body[0].1 >= HEIGHT {
			self.body[0].1 = 0;
		} else if self.body[0].0 < 0 {
			self.body[0].0 = WIDTH - 1;
		} else if self.body[0].0 >= WIDTH {
			self.body[0].0 = 0;
		}
	}

	pub unsafe fn draw(&mut self) {
		self.renderer.clear(0x000000);

		self.renderer.draw_rect(
			(self.apple.0 * TILE_SIZE + 3) as usize,
			(self.apple.1 * TILE_SIZE + 3) as usize,
			(TILE_SIZE - 5) as usize,
			(TILE_SIZE - 5) as usize,
			0x0000FF,
		);

		for i in 0..self.length {
			self.renderer.draw_rect(
				(self.body[i].0 * TILE_SIZE + 1) as usize,
				(self.body[i].1 * TILE_SIZE + 1) as usize,
				(TILE_SIZE - 1) as usize,
				(TILE_SIZE - 1) as usize,
				if i == 0 {
					0x00FFFF
				} else if i % 2 != 0 {
					0x0FFF00
				} else {
					0x39ED8D
				},
			);
		}

		self.renderer.swap_buffers();

		psp::sys::sceDisplayWaitVblankStart();
	}
}
